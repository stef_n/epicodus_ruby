# Try removing the last 4 elements of an Array.
name = ["bill", "jill", "phil", "tim", "jim", "elle", "belle"]
p name
 4.times do
 name.pop
end
 p name

# Add a number to itself several times.

one = 1
puts "This is one by itself #{one}"
5.times do
  one = one + 1
end
puts "This is one after the block #{one}"

# Use a parameter with a times loop to create an Array that looks like this: [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4]

numbers = []

3.times do
  numbers.push(0, 1, 2, 3, 4).unshift
  end
  p numbers

  # not exactly sure...

array = []
3.times do
  array << [0, 1, 2, 3, 4]
end



