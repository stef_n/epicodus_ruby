# Make an Fixnum#absolutely_larger method that will add 1
# to a number if it is positive or 0, and subtract 1 if it is negative.

class Fixnum
  define_method(:absolutely_larger) do
    if self.>=(0)
      self.+(1)
    else
      self.-(1)
    end
  end
end

  p 4.absolutely_larger
  p 0.absolutely_larger
  p (-2).absolutely_larger

# Make an Fixnum#can_drink_alcohol? method that returns a
# boolean based on if the Fixnum is greater than or equal to 21.

class Fixnum
  define_method(:can_drink_alcohol?) do
    if self.>=(21)
      puts "You can drink."
    else
      puts "No booze for you!"
    end
  end
end

(18).can_drink_alcohol?
(22).can_drink_alcohol?
# Make an Fixnum#has_two_digits? method which returns true
# if the Fixnum is between 10 and 99, or -10 and -99

# class Fixnum
#   define_method(:has_two_digits?) do
#     if self === (10..99)
#       puts "true"
#     elsif self === (-10..-99)
#       puts "true"
#     else
#       puts "false"
#     end
#   end
# end

# p (5).has_two_digits?
# p (-20).has_two_digits?
# p (45).has_two_digits?
#
# ???
