friends = ["Liz", "Sal", "Jo"]
friends.each do |friend|
  friend.concat(" loves programming")
end

puts friends

friends = ["Liz", "Sal", "Jo"]
loud_friends = []
friends.each do |friend|
  loud_friends.push(friend.upcase())
end
p loud_friends

# Loop through a list of your friends and say they are your friend (for example, "Moriah is my friend.").
friends = ["Liz", "Sal", "Jo"]
friends.each do |friend|
    friend.concat(" is my friend")
  end
puts friends

# Take a range of numbers and multiply them together.
numbers = (1..10) # could also attach this .inject(:*)

# p numbers
sum = 1
numbers.each do |number|
  sum = sum.*(number)
end

p sum

# Create a variable called my_fave_drink and set it equal to a String.
# Also create a variable to hold an empty Array and call it my_new_drink.
#  Convert my_fave_drink to an Array of single letters.
#  Now loop through each letter stored in that Array and push it into the my_new_drink Array three times.
#  Then convert my_new_drink into a String, and set my_fave_drink equal to this new String.
#   So if your favorite drink is "Pepsi", at the end of the loop it should be "PPPeeepppsssiii".

my_fave_drink = "Pepsi"
my_new_drink = []

3.times do |word|

   my_new_drink.push(my_fave_drink.split(//))


end

 puts my_new_drink.join
 #not sure how to get them by each other...
