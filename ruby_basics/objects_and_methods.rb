#String:
puts "hello world".capitalize # capitalizes
puts "HELLO WORLD".downcase #makes everything lowercase
puts "hello world".chop # removes the last character
puts "hello world".clear # removes all characters

#Fixnum:
puts 6.next # tells the next integer higher
puts 6.pred # tells the previous integer, or predecessor

#Float:
puts 8.5.ceil # rounds up
puts 6.5.floor # rounds down
puts 3.3.round
