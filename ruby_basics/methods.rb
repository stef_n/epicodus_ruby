 # Define a gimme_ten method on Fixnum that always returns 10.
 class Fixnum
   define_method(:gimme_ten) do
     10
   end
 end

 puts 3.gimme_ten

# Define a half method on Float that returns the number divided by 2.
 class Fixnum
   define_method(:half) do
     self./(2)
   end
 end

 puts 4.half

 # Define a next_in_line method on Array that takes the element at the beginning of the array and puts it at the end.
class Array
   define_method(:next_in_line) do
      self.push.shift
   end
end

 puts [1, 2, 3, 4].next_in_line
