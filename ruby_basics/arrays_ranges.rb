# Array:
p [1, 2, 3, 4, 5,].unshift("a") # takes an argument and adds it to the beginning of the array
p [1, 2, 3, 4, 5,].shift(3) # removes the first element of the array and returns it
p [1, 2, 3, 4, 5,].reverse() # reverses the order of the elements
p [1, 2, 3, 4, 5,].pop() # removes the last element of the array and returns it
p [1, 2, 3, 4, 5,].join()
p [1, 2, 3, 4, 5,].sample(4)
p [1, 2, 3, 3, 4, 4, 5,].uniq()
p [1, 2, 3, 4, 5,].shuffle()

# Range:
p (1..10).size
p (1..10).begin
p ("a".."z").end
