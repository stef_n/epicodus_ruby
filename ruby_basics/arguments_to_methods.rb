# String:
puts "hello".center(4) # takes an integer as an argument and centers the string within that many spaces - make sure the argument is big enough to see how it works
puts "ohmy".concat("GOSH") # takes a string as an argument and concatenates the two strings
puts ("string").count("s") # takes a string as an argument and counts how many times the characters of the argument string appear in the original string (this one can be tricky when using more than one character in the argument.  See if you can figure out how it's operating).
puts "Here is a string".delete("string") # takes a string as an argument and deletes any occurrences of that string from the original string

# Fixnum and Float:
puts 3 + (2) # takes an argument of another number and adds the two
puts 3 - (2)
puts 3 * (2)
puts 3 / (2)
puts 3 % (2) # takes an argument of another number, divides the two, and gives the remainder
