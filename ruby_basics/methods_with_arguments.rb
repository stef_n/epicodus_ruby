# Define a subtract method on Fixnum.

  class Fixnum
    define_method(:subtract) { |n| self.-(n) }
  end

  puts 5.subtract(3)

# Define a combine method on String that works like concat, so that you can run something like "break".combine("fast").

class String
  define_method(:combine) { |w| self.+(w) }
end

puts "break".combine("fast")
